# Argument 1 ($1) is the SD Card ROOT/ filepath
sudo rsync -a boost_bin/usr_local_include/ $1/usr/local/include/
sudo rsync -a boost_bin/usr_local_lib/ $1/usr/local/lib/

# hmmm this changes group:user of /usr/local/include and /usr/local/lib
# from eyounge:eyounge
# to root:root
# ... not sure if this is a problem yet...
