# Argument 1 ($1) is the SD Card ROOT/ filepath

# git
sudo cp git_bin/* $1/usr/bin/
sudo cp -r git_libexec/libexec $1/usr/
sudo rsync -a git_share/ $1/usr/share/

# vim
sudo cp vim_bin/* $1/usr/local/bin/
sudo cp -r vim_share/vim $1/usr/local/share

# ssh
sudo cp ssh_bin/* $1/usr/local/bin/
sudo cp ssh_sbin/* $1/usr/local/sbin/
sudo mkdir $1/usr/local/libexec
sudo cp ssh_libexec/* $1/usr/local/libexec/

# boost - updating to threaded version
sudo rsync -a boost_bin_threaded/usr_local_include/ $1/usr/local/include/
sudo rsync -a boost_bin_threaded/usr_local_lib/ $1/usr/local/lib/
echo "/usr/local/lib" >> $1/etc/ld.so.conf
#  * Note that user will still need to run ldconfig command on ZYNQ script for libary to be added...

# EOF
